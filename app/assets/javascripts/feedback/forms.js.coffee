$(document).on "ready page:load", ->
  $('.ui.accordion').accordion('open', 0)

  $('form').on 'click', '.add_fields', (event) ->
    time = new Date().getTime()
    regexp = new RegExp($(this).data('id'), 'g')
    $(this).closest("div").before($(this).data('fields').replace(regexp, time))
    event.preventDefault()

  $(document).on 'click', '.ui.feedback.rating', (event) ->
    all_done = $(this).rating('get rating')

    $(this).siblings("input").first().val($(this).rating('get rating'))
    $(this).parents().eq(1).siblings().each ->
      $(this).children().each ->
        $(this).children('.ui.feedback.rating').each ->
          all_done *= $(this).rating('get rating')
    if all_done
      $(this).parents().eq(3).siblings(".ui.button").each ->
        $(this).prop("disabled", false).removeClass("negative").addClass("positive")

  $(document).on 'click', '.ui.button', (event) ->
    $('.ui.accordion').accordion('open', parseInt($(this).parents().eq(2).attr("data-index")) + 1)
    $(this).parents().eq(2).hide()