require_dependency "feedback/application_controller"

module Feedback
  class FormsController < ApplicationController
    before_action :set_form, only: [:show, :edit, :update, :destroy]

    # GET /forms
    def index
      @forms = policy_scope(Form.all)
      authorize @forms
    end

    # GET /forms/1
    def show
    end

    # GET /forms/new
    def new
      @form = Form.new
      authorize(@form)
    end

    # GET /forms/1/edit
    def edit
    end

    # POST /forms
    def create
      @form = Form.new(form_params)
      authorize(@form)
      
      if @form.save
        redirect_to forms_url, notice: 'Form was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /forms/1
    def update
      if @form.update(form_params)
        redirect_to @form, notice: 'Form was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /forms/1
    def destroy
      @form.destroy
      redirect_to forms_url, notice: 'Form was successfully destroyed.'
    end

    def questions
      @form = Form.find(params[:form_id])
      authorize(@form)
    end

    def submission
      @form = Form.find(params[:form_id])
      authorize(@form)
      @student = Student.find_by(user: current_user)
      
      respond_to do |format|
        format.html do
          @teachers = []
          @subjects = @student.batch.subjects
          @subjects.each do |subject|
            subject.teachers.each do |teacher|
              answers = Answer.where(form: @form, student: @student, 
                teacher: teacher, subject: subject)
              @teachers << {id: teacher.id, subject_id: subject.id,
                name: teacher.to_s, subject: subject.to_s} if answers.empty?
            end
          end
        end

        format.js do
          @teacher = Employee.find(form_params[:teacher_id])
          @subject = Subject.find(form_params[:subject_id])
          form_params[:question_ids].each_with_index do |question, index|
            @question = Question.find(question)
            Answer.create!(form: @form, question: @question, student: @student,
              batch: @student.batch, teacher: @teacher, subject: @subject, 
              points: form_params[:question_points][index])
          end
          Remark.create!(form: @form, student: @student, 
            teacher: @teacher, content: form_params[:remark]) unless form_params[:remark].nil?
        end
      end
    end

    def batches
      @form = Form.find(params[:form_id])
      authorize(@form)
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_form
        @form = Form.find(params[:id])
        authorize(@form)
      end

      # Only allow a trusted parameter "white list" through.
      def form_params
        params.require(:form).permit(:creator_id, :name, :start_date, 
          :end_date, :teacher_id, :subject_id, :remark, batch_ids: [],
          sections_attributes: [:id, :title, questions_attributes: [:id, :content]],
          question_ids:[], question_points:[])
      end
  end
end
