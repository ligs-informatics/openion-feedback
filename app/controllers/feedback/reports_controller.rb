require_dependency "feedback/application_controller"

module Feedback
  class ReportsController < ApplicationController
    def by_teacher
      authorize Form.find(params[:form_id])
      @teacher = current_user
      @subjects = Feedback::Answer.where(form_id: 1, teacher_id: 2)
      .group(:subject_id).select(:subject_id).map{ |answer| answer.subject}
    end

    def by_form
      @form = Form.find(params[:form_id])
      authorize @form

      @statistics = []
      @form.questions.each do |question|
        subjects = []
        Feedback::Answer.where(form_id: @form, question: question)
        .group(:teacher_id, :subject_id).average(:points).each do |subject, average|
        subjects << [
          [Employee.find(subject[0]).to_s,
          Subject.find(subject[1]).to_s],
          average
          ]
        end
        @statistics << {question: question.content, data: subjects}
      end
    end
  end
end
