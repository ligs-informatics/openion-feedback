module Feedback
  class Answer < ActiveRecord::Base
    belongs_to :form
    belongs_to :question
    belongs_to :student
    belongs_to :batch
    belongs_to :teacher, class_name: "Employee"
    belongs_to :subject
  end
end
