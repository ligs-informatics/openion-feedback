module Feedback
  class Form < ActiveRecord::Base
    belongs_to :creator, class_name: "User"
    has_many   :sections
    has_many   :questions, through: :sections
    has_and_belongs_to_many :batches, class_name: "Batch"

    validates :name, presence: true
    validates :name, uniqueness: { scope: :creator }
    validates :start_date, presence: true
    validates :end_date, presence: true

    accepts_nested_attributes_for :sections
  end
end
