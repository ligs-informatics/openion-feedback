module Feedback
  class Question < ActiveRecord::Base
    belongs_to :section
    has_many   :answers

    def to_s
      content
    end
  end
end
