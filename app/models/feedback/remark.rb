module Feedback
  class Remark < ActiveRecord::Base
    belongs_to :form
    belongs_to :student
    belongs_to :teacher, class_name: "Employee"
  end
end
