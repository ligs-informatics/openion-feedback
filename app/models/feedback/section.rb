module Feedback
  class Section < ActiveRecord::Base
    belongs_to :form
    has_many   :questions
    
    accepts_nested_attributes_for :questions
  end
end
