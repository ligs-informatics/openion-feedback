Feedback::Engine.routes.draw do
  resources :forms do
    get 'questions'
    get 'batches'
    match 'submission', via: [:get, :patch]

    get 'reports/by_teacher'
    get 'reports/by_form'
  end
end
