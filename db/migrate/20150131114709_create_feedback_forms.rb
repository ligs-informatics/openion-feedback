class CreateFeedbackForms < ActiveRecord::Migration
  def change
    create_table :feedback_forms do |t|
      t.belongs_to :creator
      t.string     :name
      t.date       :start_date
      t.date       :end_date

      t.timestamps null: false
    end
  end
end
