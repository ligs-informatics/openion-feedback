class CreateFeedbackSections < ActiveRecord::Migration
  def change
    create_table :feedback_sections do |t|
      t.belongs_to :form
      t.string :title

      t.timestamps null: false
    end
  end
end
