class CreateFeedbackQuestions < ActiveRecord::Migration
  def change
    create_table :feedback_questions do |t|
      t.belongs_to :section
      t.string :content

      t.timestamps null: false
    end
  end
end
