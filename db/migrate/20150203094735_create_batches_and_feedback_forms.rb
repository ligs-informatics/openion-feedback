class CreateBatchesAndFeedbackForms < ActiveRecord::Migration
  def change
    create_table :batches_feedback_forms, id: false do |t|
      t.belongs_to :batch, index: true
      t.belongs_to :form, index: true
    end
  end
end
