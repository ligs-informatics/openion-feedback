class CreateFeedbackAnswers < ActiveRecord::Migration
  def change
    create_table :feedback_answers do |t|
      t.belongs_to :form, index: true
      t.belongs_to :question, index: true
      t.belongs_to :student, index: true
      t.belongs_to :batch, index: true
      t.belongs_to :teacher, index: true
      t.belongs_to :subject, index: true
      t.integer :points

      t.timestamps null: false
    end
  end
end
