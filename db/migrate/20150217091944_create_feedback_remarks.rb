class CreateFeedbackRemarks < ActiveRecord::Migration
  def change
    create_table :feedback_remarks do |t|
      t.belongs_to :form, index: true
      t.belongs_to :student, index: true
      t.belongs_to :teacher, index: true
      t.text :content

      t.timestamps null: false
    end
  end
end
