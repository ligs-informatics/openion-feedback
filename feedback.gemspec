$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "feedback/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "feedback"
  s.version     = Feedback::VERSION
  s.authors     = ["Kevin Madhu"]
  s.email       = ["kevin.madhu@gmail.com"]
  s.homepage    = "https://gitlab.com/ligs-informatics/openion-feedback/wikis/home"
  s.summary     = "Feedback-plugin"
  s.description = "This is a feedback plugin implementation for OpenionERP."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.2.0"

  s.add_development_dependency "sqlite3"
end
