require 'test_helper'

module Feedback
  class FormSectionsControllerTest < ActionController::TestCase
    setup do
      @form_section = form_sections(:one)
    end

    test "should get index" do
      get :index
      assert_response :success
      assert_not_nil assigns(:form_sections)
    end

    test "should get new" do
      get :new
      assert_response :success
    end

    test "should create form_section" do
      assert_difference('FormSection.count') do
        post :create, form_section: { form_id: @form_section.form_id, heading: @form_section.heading }
      end

      assert_redirected_to form_section_path(assigns(:form_section))
    end

    test "should show form_section" do
      get :show, id: @form_section
      assert_response :success
    end

    test "should get edit" do
      get :edit, id: @form_section
      assert_response :success
    end

    test "should update form_section" do
      patch :update, id: @form_section, form_section: { form_id: @form_section.form_id, heading: @form_section.heading }
      assert_redirected_to form_section_path(assigns(:form_section))
    end

    test "should destroy form_section" do
      assert_difference('FormSection.count', -1) do
        delete :destroy, id: @form_section
      end

      assert_redirected_to form_sections_path
    end
  end
end
