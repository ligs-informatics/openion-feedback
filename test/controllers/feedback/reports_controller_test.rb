require 'test_helper'

module Feedback
  class ReportControllerTest < ActionController::TestCase
    test "should get by_teacher" do
      get :by_teacher
      assert_response :success
    end

    test "should get by_all" do
      get :by_all
      assert_response :success
    end

  end
end
